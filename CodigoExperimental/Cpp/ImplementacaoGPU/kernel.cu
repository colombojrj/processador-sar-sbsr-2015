/*
 * Este c�digo escrito em C++ implementa os dois la�os "for" presentes no
 * arquivo polsar_h_a_alfa.pro, do Projeto AEROSAR, de forma que o fruto 
 * desta compila��o seja um arquivo DLL, que pode ser chamado pelo programa
 * IDL, diminuindo o tempo de execu��o do algoritmo. � importante salientar
 * que o processamento � realizado na GPU.
 *
 * Autor do algoritmo C++: Jos� Roberto Colombo Junior
 * Autor do algoritmo IDL: Naiallen Carolyne Rodrigues Lima
 * Coordenador do projeto: David Fernandes
 *
 * Data: 23/07/2014
 * Local: ITA
 *
 */

// O valor desse log foi obtido no WolframAlpha (https://www.wolframalpha.com/input/?i=log10%283.0%29)
#define LOG10_3 0.4771212547196624372950279032551153092001288641906958
// O valor dessa convers�o de rad para degree foi obtido em https://www.wolframalpha.com/input/?i=180%2Fpi
#define R2D 57.295779513082320876798154814105170332405472466564321

#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include "idl_export.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

// Fun��o para multiplicar dois n�meros complexos
// n1 = n�mero 1
// n2 = n�mero 2
// res= resultado
__device__ void c_mul (IDL_COMPLEX *n1, IDL_COMPLEX *n2, IDL_COMPLEX *res) {
	res->r = n1->r * n2->r - n1->i * n2->i;
	res->i = n1->r * n2->i + n1->i * n2->r;
}

// Fun��o para dividir dois n�meros complexos (n1/n2)
// n1 = n�mero 1
// n2 = n�mero 2
// res= resultado
__device__ void c_div (IDL_COMPLEX *n1, IDL_COMPLEX *n2, IDL_COMPLEX *res) {
	float den = (n2->r)*(n2->r) + (n2->i)*(n2->i);
	res->r = ( n1->r * n2->r + n1->i * n2->i) / den;
	res->i = (-n1->r * n2->i + n1->i * n2->r) / den;
}

// Fun��o para somar dois n�meros complexos
// n1 = n�mero 1
// n2 = n�mero 2
// res= resultado
__device__ void c_som (IDL_COMPLEX *n1, IDL_COMPLEX *n2, IDL_COMPLEX *res) {
	res->r = n1->r + n2->r;
	res->i = n1->i + n2->i;
}

// Fun��o para subtrair dois n�meros complexos
// n1 = n�mero 1
// n2 = n�mero 2
// res= resultado
__device__ void c_sub (IDL_COMPLEX *n1, IDL_COMPLEX *n2, IDL_COMPLEX *res) {
	res->r = n1->r - n2->r;
	res->i = n1->i - n2->i;
}

// Fun��o para calcular o m�dulo de um n�mero complexo
// num = n�mero complexo
// res = resultado
__device__ float c_abs (IDL_COMPLEX *num, int raiz) {
	if (raiz == 1) {
		return sqrt((num->r)*(num->r) + (num->i)*(num->i));
	} else {
		return (num->r)*(num->r) + (num->i)*(num->i);
	}
}

// Fun��o para calcular os autovetores da matriz A
// A = matriz quadrada (IDL_COMPLEX)
// D = vetor com os autovalores da matriz A
// V = matriz com os autovetores da matriz A
__device__ void autovetores (IDL_COMPLEX *A, float *D, IDL_COMPLEX *V, int x_size, int normalizar) {
	// Aloca algumas vari�veis auxiliares
    IDL_COMPLEX AL[9], v1[3], v2[3]; // vari�veis auxiliares
	int n, l, c, i;                  // indexadores
	int l1, l2;                      // linhas a serem escalonadas

	// Para cada autovalor
	for (n = 0; n < x_size; n++) {
		// Aloca (A-LAMBDA*I) em V_temp
		for (l = 0; l < 3; l++) {
			for (c = 0; c < 3; c++) {
				if (l == c) {
					AL[c + l*x_size].r = A[c + l*x_size].r -D[n];
				} else {
					AL[c + l*x_size].r = A[c + l*x_size].r;
				}
				AL[c + l*x_size].i = A[c + l*x_size].i;
			}
		}

		// Faz o escalonamento de duas linhas da matriz AL, com o objetivo de eliminar uma
		// das tr�s vari�veis
		if (c_abs(&AL[0], 0) > 1e-3) { // Se o priemiro elemento da primeira linha n�o for zero
			l1 = 0;
			if (c_abs(&AL[3], 0) > 1e-3) {
				l2 = 1;
			} else {
				l2 = 2;
			}
		} else {
			l1 = 1;
			l2 = 2;
		}
		
		for (c = 0; c < x_size; c++) {
			c_mul (&AL[l1*x_size], &AL[c + x_size], &v1[c]);
			c_div (&v1[c], &AL[l2*x_size], &v2[c]);
			// Subtrai a linha escalonada com a primeira linha da matriz AL
			c_sub (&AL[c], &v2[c], &v1[c]);
		}

		// Existem infinitas solu��es para os autovetores. Para encontrar uma delas, 
		// pode-se escolher a vari�vel v3 como sendo 1+j0
		V[n + 2*x_size].r = 1;
		V[n + 2*x_size].i = 0;

		// Encontra v2, substituindo v3 (== 1) na equa��o escalonada (que s� tem v2 e v3)
		c_mul (&v1[2], &V[n + 2*x_size], &v1[2]); // multiplica por v3
		c_div (&v1[2], &v1[1], &V[n + x_size]);
		V[n + x_size].r = V[n + x_size].r *-1; // Tem que trocar o sinal, pois v2
		V[n + x_size].i = V[n + x_size].i *-1; // vai para o outro lado da equa��o

		// Com os valores de v2 e v3, resta encontrar v1
		c_mul (&AL[l1*x_size +1], &V[n + x_size], &v1[0]);
		c_mul (&AL[l1*x_size +2], &V[n + 2*x_size], &v1[1]);
		c_som (&v1[0], &v1[1], &v1[2]);
		c_div (&v1[2], &AL[l1*x_size], &V[n]); 
		V[n].r = V[n].r * -1;
		V[n].i = V[n].i * -1;	

		//
		// Faz a normaliza��o do autovetor em quest�o
		//
		
		if (normalizar == 1) {
			// Calcula o m�dulo do vetor
			v1[0].r = 0; // Estou usando v1[0] como vari�vel auxiliar aqui...
			v1[0].i = 0;
			for (c = 0; c < x_size; c++) {
				v1[0].r = v1[0].r + c_abs(&V[n + c*x_size], 0);
			}
			// Calcula a raiz quadrada desse valor
			v1[0].r = sqrt (v1[0].r);

			// Divide o vetor por esse n�mero para normaliz�-lo
			for (c = 0; c < x_size; c++) {
				c_div(&V[n + c*x_size], &v1[0], &V[n + c*x_size]);
			}
		}
	}
}

// Fun��o f(x)
__device__ double f(double x, double *c) {
	return x*x*x + c[0]*x*x + c[1]*x + c[2];
}

// Fun��o f'(x)
__device__ double fd(double x, double *c) {
	return 3*x*x + c[0]*2*x + c[1];
}

// c = vetor contendo os coeficientes
// x = vetor contendo as ra�zes do polinomio (sa�da)
__device__ int solve(float *c, float *x) {
	//
	//                                    f(x(n))
	// Newton's method: x(n+1) = x(n) - ----------
	//                                   f'(x(n))
	//

	// Configura��o do otimizador
	int n_max = 100;   // n�mero m�ximo de itera��es
	double tol = 1e-5; // toler�ncia admitida (crit�rio de parada)

	// Vari�veis em double (x double)
    double cld[3], xld[3];

	// Copia o c para cld
	cld[0] = c[0];
	cld[1] = c[1];
	cld[2] = c[2];

	// Cria um palpite inicial com base na solu��o de uma equa��o de 2nd ordem
	double x1 = (-cld[0] +sqrt(cld[0]*cld[0] -3*cld[1]))/3;
	double x2 = (-cld[0] -sqrt(cld[0]*cld[0] -3*cld[1]))/3;
	
	if (x1 > x2) {
		xld[0] = x1 +0.1;
		xld[1] = (x1 + x2) / 2;
		xld[2] = x2 -0.1;
	} else {
		xld[0] = x1 -0.1;
		xld[1] = (x1 + x2) / 2;
		xld[2] = x2 +0.1;
	}

	int raiz, n;        // indice
	double erro;   // erro
	double der;    // valor da derivada da fun��o para algum x
	double rel;    // valor do relaxamento

	// Paraleliza o loop (calcula as 3 ra�zes ao mesmo tempo)
	for (raiz = 0; raiz < 3; raiz++) {
		erro = 1000;
		n = 0;
		while ((abs(erro) > tol) && (n < n_max)) {
			//x[raiz] = x[raiz] -f(x[raiz],c)/fd(x[raiz],c);
			
			// Testa se a derivada da fun��o est� pr�xima do zero,
			// se estiver, introduzir� um relaxamento na divis�o
			der = fd(xld[raiz], cld);
			if (abs(der) < 1e-3) {
				// Introduz um relaxamento no fd
				if (der < 0) {
					rel = -0.1;
				} else {
					rel = 0.1;
				}				
				xld[raiz] = xld[raiz] -f(xld[raiz], cld)/(der +rel);
			} else {
				xld[raiz] = xld[raiz] -f(xld[raiz], cld)/der;
			}
			
			// Crit�rio de parada sobre o qu�o longe a ra�z est� do zero
			erro = f(xld[raiz], cld);
			n = n +1;
		}
	}

	// Copia os resultados
	x[0] = (float) xld[0];
	x[1] = (float) xld[1];
	x[2] = (float) xld[2];

	return 0;
}

// Fun��o para calcular os autovalores de uma matriz
// A      -> matriz hermitiana
// D      -> vetor real para armazenar os autovalores
__device__ void autovalores (IDL_COMPLEX *A, float *D) {
    float c[3];                 // Coeficientes do polin�mio de grau 3
    IDL_COMPLEX n1, n2, A_m[9]; // vari�veis auxiliares
	int n;                      // indexador

	for (n = 0; n < 9; n++) {
		A_m[n].r = A[n].r * 1e-3;
		A_m[n].i = A[n].i * 1e-3;
	}

	// C�lculo do primeiro coeficiente
	// a0 + a4 + a8 (apenas n�meros reais)
	c[0] = -(A_m[0].r + A_m[4].r + A_m[8].r);

	// C�lculo do segundo coeficiente
	c[1] = (A_m[0].r + A_m[4].r) * A_m[8].r + A_m[0].r*A_m[4].r;
	c_mul (&A_m[2], &A_m[6], &n1);
	c[1] = c[1] - n1.r;
	c_mul (&A_m[5], &A_m[7], &n1);
	c[1] = c[1] - n1.r;
	c_mul (&A_m[1], &A_m[3], &n1);
	c[1] = c[1] - n1.r;

	// C�lculo do terceiro coeficiente
	c[2] = -A_m[0].r * A_m[4].r * A_m[8].r;
	c_mul (&A_m[1], &A_m[6], &n1);
	c_mul (&n1, &A_m[5], &n1);
	c_mul (&A_m[3], &A_m[2], &n2);
	c_mul (&n2, &A_m[7], &n2);
	c_som (&n1, &n2, &n1);
	c[2] = c[2] -n1.r;
	c_mul (&A_m[2], &A_m[6], &n1);
	c[2] = c[2] +n1.r * A_m[4].r;
	c_mul (&A_m[5], &A_m[7], &n1);
	c[2] = c[2] +n1.r * A_m[0].r;
	c_mul (&A_m[1], &A_m[3], &n1);
	c[2] = c[2] +n1.r * A_m[8].r;

	// Resolve a equa��o
	solve(c, D);

	// Multiplica os autovalores por 1000
	for (n = 0; n < 3; n++) {
		D[n] = D[n] * 1e3 ;
	}
}

__device__ void ordenar (float *vetor, IDL_COMPLEX *V, IDL_LONG x_size) {
	float temp;
	int i, j, k;
	IDL_LONG fim = x_size -(IDL_LONG) 1;
	float t1, t2;

	for (i = 0; i < fim; i++) {
		for (j = 0; j < fim; j++) {
			if (vetor[j] < vetor[j+1]) {
				// Faz o c�lculo para cada autovalor
				temp = vetor[j];
				vetor[j] = vetor[j+1];
				vetor[j+1] = temp;

				// Altera tamb�m a ordem dos autovetores
				for (k = 0; k < 3; k++) {
					t1 = V[j + x_size*k].r;
					t2 = V[j + x_size*k].i;

					V[j + x_size*k].r = V[j+1 + x_size*k].r;
					V[j + x_size*k].i = V[j+1 + x_size*k].i;

					V[j+1 + x_size*k].r = t1;
					V[j+1 + x_size*k].i = t2;
				}
			}
		}
	}
}

// Uma �nica fun��o que executa toda a opera��o, chamando fun��es menores
__global__ void polsar_h_a_alfa_kernel (IDL_COMPLEX *k0_d, IDL_COMPLEX *k1_d,  IDL_COMPLEX *k2_d,
    //float *lamb1_d, float *lamb2_d, float *lamb3_d, 
	float *alfa_d, float *entropia_d, 
    float *anisotropia_d, IDL_LONG dimx, IDL_LONG dimy, IDL_LONG i_start, IDL_LONG j_start, 
    IDL_LONG i_fim, IDL_LONG j_fim, IDL_LONG jan1, IDL_LONG jan2) {

    // Vari�veis indexadoras para o loop mais geral
	IDL_LONG i, j;

    i = blockIdx.x * blockDim.x + threadIdx.x;
    j = blockIdx.y * blockDim.y + threadIdx.y;

    // S� faz o c�lculo para os elementos dentro da faixa correta
    if ((i >= i_start) && (i <= i_fim)) {
    if ((j >= j_start) && (j <= j_fim)) {
    
    // Vari�veis para acessar a matriz	
	IDL_LONG x, y;
	
	// Faz a somat�ria (para o c�lculo da m�dia)
	float elementos = (2*jan1+1)*(2*jan2+1);
	IDL_COMPLEX mc[9];

	// Vari�veis auxiliares
	float temp;
	int k; // indexador

	// Aloca espa�o para os autovalores e autovetores da matriz mc
	IDL_COMPLEX V[9]; // matriz contendo os autovetores
	float D[3];       // vetor contendo os autovalores
    float soma_autovalores;              

	// Para o c�lculo de alfa, entropia e anisotropia
	float PP[3], ent_a = 0;
	int ia;

	// Inicia os n�meros complexos com zero
	for (k = 0; k < 9; k++) {
		mc[k].r = 0; mc[k].i = 0;
		V[k].r = 0; V[k].i = 0;
	}

	// Loop sobre as matrizes K0, K1 e K2
	for (x = i-jan2; x <= (i+jan2); x++) {
		for (y = j-jan1; y <= (j+jan1); y++) {
			// A00
			temp = c_abs( &k0_d[x + y*dimx], 1);
			mc[0].r = mc[0].r + temp*temp;

			// A01
			mc[1].r = mc[1].r + k0_d[x + y*dimx].r * k1_d[x + y*dimx].r; // parte real
			mc[1].r = mc[1].r + k0_d[x + y*dimx].i * k1_d[x + y*dimx].i; 
			mc[1].i = mc[1].i + k0_d[x + y*dimx].i * k1_d[x + y*dimx].r; // parte imagin�ria
			mc[1].i = mc[1].i - k0_d[x + y*dimx].r * k1_d[x + y*dimx].i;

			// A02
			mc[2].r = mc[2].r + k0_d[x + y*dimx].r * k2_d[x + y*dimx].r; // parte real
			mc[2].r = mc[2].r + k0_d[x + y*dimx].i * k2_d[x + y*dimx].i; 
			mc[2].i = mc[2].i + k0_d[x + y*dimx].i * k2_d[x + y*dimx].r; // parte imagin�ria
			mc[2].i = mc[2].i - k0_d[x + y*dimx].r * k2_d[x + y*dimx].i;

			// A10 (� apenas o conjugado de A01)

			// A11
			temp = c_abs(&k1_d[x + y*dimx], 1);
			mc[4].r = mc[4].r + temp*temp;

			// A12
			mc[5].r = mc[5].r + k1_d[x + y*dimx].r * k2_d[x + y*dimx].r; // parte real
			mc[5].r = mc[5].r + k1_d[x + y*dimx].i * k2_d[x + y*dimx].i; 
			mc[5].i = mc[5].i + k1_d[x + y*dimx].i * k2_d[x + y*dimx].r; // parte imagin�ria
			mc[5].i = mc[5].i - k1_d[x + y*dimx].r * k2_d[x + y*dimx].i;

			// A20 (� apenas o conjugado de A02)
			// A21 (� apenas o conjugado de A12)

			// A22 
			temp = c_abs(&k2_d[x + y*dimx], 1);
			mc[8].r = mc[8].r + temp*temp;
		}
	}
	
	// Finaliza o c�lculo da m�dia
	mc[0].r = mc[0].r / elementos;
	mc[0].i = 0;
	mc[1].r = mc[1].r / elementos;
	mc[1].i = mc[1].i / elementos;
	mc[2].r = mc[2].r / elementos;
	mc[2].i = mc[2].i / elementos;
	mc[3].r = mc[1].r;
	mc[3].i = -1 * mc[1].i;
	mc[4].r = mc[4].r / elementos;
	mc[4].i = 0;
	mc[5].r = mc[5].r / elementos;
	mc[5].i = mc[5].i / elementos;
	mc[6].r = mc[2].r;
	mc[6].i = -1 * mc[2].i;
	mc[7].r = mc[5].r;
	mc[7].i = -1 * mc[5].i;
	mc[8].r = mc[8].r / elementos;
	mc[8].i = 0;

	// Calcula os autovalores e os autovetores da matriz mc
	autovalores (mc, D);
	autovetores (mc, D, V, 3, 1);
	
	// Ordena os autovalores
	ordenar (D, V, 3);

    // Aloca os autovalores nas matrizes do IDL
	//lamb1_d[i + j*dimx] = D[0];
	//lamb2_d[i + j*dimx] = D[1];
	//lamb3_d[i + j*dimx] = D[2];

    // Soma os autovalores
	soma_autovalores = D[0] + D[1] + D[2];

    //
	// C�lculo da entropia

    // Aloca o vetor PP
	PP[0] = D[0] / soma_autovalores;
	PP[1] = D[1] / soma_autovalores;
	PP[2] = D[2] / soma_autovalores;

	ent_a = 0;
	for (ia = 0; ia < 3; ia++) {
		if (PP[ia] > 0) {
			ent_a =  ent_a -PP[ia] * log10(PP[ia])/LOG10_3;
		}
	}
	entropia_d[i + j*dimx] = ent_a;

	// C�lculo da anisotropia
	anisotropia_d[i + j*dimx] = (D[1]-D[2])/(D[1]+D[2]);

	//
	// C�lculo do alfa
	alfa_d[i + j*dimx] = 0;
	for (ia = 0; ia < 3; ia++) {
		alfa_d[i + j*dimx] = alfa_d[i + j*dimx] + PP[ia]*acos(c_abs(&V[ia], 1));
	}
	alfa_d[i + j*dimx] = alfa_d[i + j*dimx] * R2D;

    }
    }
}

//
// ESTA FUN��O N�O � MAIS UTILIZADA! ELA SER� MANTIDA PORQUE TALVEZ NO FUTURO ELA SEJA NECESS�RIA
//
//
// Fun��o para fazer a classifica��o da imagem na GPU
__global__ void classificacao_h_alfa_kernel (IDL_LONG ja_start, IDL_LONG ja_fim, IDL_LONG ih_start, 
	IDL_LONG ih_fim, IDL_LONG dimx, IDL_LONG dimy, float *alfa_d, float *entropia_d, 
	float *anisotropia_d, IDL_INT *imagem_h_a_0_d, IDL_INT *imagem_h_a_1_d, IDL_INT *imagem_h_a_2_d,
	IDL_INT *imagem_h_a_ani_0_d, IDL_INT *imagem_h_a_ani_1_d, IDL_INT *imagem_h_a_ani_2_d) {
    
	// Vari�veis indexadoras para o loop mais geral
	IDL_LONG ih, ja;

    ih = blockIdx.x * blockDim.x + threadIdx.x;
    ja = blockIdx.y * blockDim.y + threadIdx.y;

    // S� faz o c�lculo para os elementos dentro da faixa correta
	if ((ih >= ih_start) && (ih <= ih_fim)) {
		if ((ja >= ja_start) && (ja <= ja_fim)) {

		// Zona 9
		if (entropia_d[ih +ja*dimx] <= 0.5 && alfa_d[ih +ja*dimx] <= 42.5) {
			imagem_h_a_0_d[ih +ja*dimx] = 255;
			imagem_h_a_1_d[ih +ja*dimx] = 255;
			imagem_h_a_2_d[ih +ja*dimx] = 0;  
		}
	
		// Zona 8
		if ((entropia_d[ih +ja*dimx] <= 0.5) && (alfa_d[ih +ja*dimx] >= 42.5) && (alfa_d[ih +ja*dimx] <= 47.5)) {
			imagem_h_a_0_d[ih +ja*dimx] = 255;
			imagem_h_a_1_d[ih +ja*dimx] = 0;
			imagem_h_a_2_d[ih +ja*dimx] = 255;
		}

		// Zona 7
		if ((entropia_d[ih +ja*dimx] <= 0.5) && (alfa_d[ih +ja*dimx] >= 47.5)) {

			imagem_h_a_0_d[ih +ja*dimx] = 0;
			imagem_h_a_1_d[ih +ja*dimx] = 255;
			imagem_h_a_2_d[ih +ja*dimx] = 255;
		}

		// Zona 6
		if ((entropia_d[ih +ja*dimx] >= 0.5) && (entropia_d[ih +ja*dimx] <= 0.9) && 
			(alfa_d[ih +ja*dimx] <= 40)) {

			imagem_h_a_0_d[ih +ja*dimx] = 255;
			imagem_h_a_1_d[ih +ja*dimx] = 0;
			imagem_h_a_2_d[ih +ja*dimx] = 0;
		}

		// Zona 5
		if ((entropia_d[ih +ja*dimx] >= 0.5) && (entropia_d[ih +ja*dimx] <= 0.9) && 
			(alfa_d[ih +ja*dimx] >= 40) && (alfa_d[ih +ja*dimx] <= 50)) {

			imagem_h_a_0_d[ih +ja*dimx] = 0;
			imagem_h_a_1_d[ih +ja*dimx] = 255;
			imagem_h_a_2_d[ih +ja*dimx] = 0;
		}

		// Zona 4
		if ((entropia_d[ih +ja*dimx] >= 0.5) && (entropia_d[ih +ja*dimx] <= 0.9) &&
			(alfa_d[ih +ja*dimx] >= 50)) {

			imagem_h_a_0_d[ih +ja*dimx] = 0;
			imagem_h_a_1_d[ih +ja*dimx] = 0;
			imagem_h_a_2_d[ih +ja*dimx] = 255;
		}

		// Zona 2
		if ((entropia_d[ih +ja*dimx] >= 0.9) && (alfa_d[ih +ja*dimx] >= 40) && 
			(alfa_d[ih +ja*dimx] <= 55)) {

			imagem_h_a_0_d[ih +ja*dimx] = 255;
			imagem_h_a_1_d[ih +ja*dimx] = 255;
			imagem_h_a_2_d[ih +ja*dimx] = 255;
		}

		// Zona 1
		if ((entropia_d[ih +ja*dimx] >= 0.9) && (alfa_d[ih +ja*dimx] >= 55)) {
			imagem_h_a_0_d[ih +ja*dimx] = 0;
			imagem_h_a_1_d[ih +ja*dimx] = 0;
			imagem_h_a_2_d[ih +ja*dimx] = 0;
		}

		////////////////////////////////////////////////////
		// CLASSIFICA��O USANDO ENTROPIA-ALFA-ANISOTROPIA //
		////////////////////////////////////////////////////

	if (anisotropia_d[ih +ja*dimx] >= 0.5) {
		if (imagem_h_a_0_d[ih +ja*dimx] == 255) imagem_h_a_ani_0_d[ih +ja*dimx] = 191;
		if (imagem_h_a_0_d[ih +ja*dimx] == 0)   imagem_h_a_ani_0_d[ih +ja*dimx] =  64;
		if (imagem_h_a_1_d[ih +ja*dimx] == 255) imagem_h_a_ani_1_d[ih +ja*dimx] = 191;
		if (imagem_h_a_1_d[ih +ja*dimx] == 0)   imagem_h_a_ani_1_d[ih +ja*dimx] =  64;
		if (imagem_h_a_2_d[ih +ja*dimx] == 255) imagem_h_a_ani_2_d[ih +ja*dimx] = 191;
		if (imagem_h_a_2_d[ih +ja*dimx] == 0)   imagem_h_a_ani_2_d[ih +ja*dimx] =  64;
	}
    if (anisotropia_d[ih +ja*dimx] <= 0.5) {
		imagem_h_a_ani_0_d[ih +ja*dimx] = imagem_h_a_0_d[ih +ja*dimx];
		imagem_h_a_ani_1_d[ih +ja*dimx] = imagem_h_a_1_d[ih +ja*dimx];
		imagem_h_a_ani_2_d[ih +ja*dimx] = imagem_h_a_2_d[ih +ja*dimx];
	}	

		}
	}
}

//
// ESTA FUN��O N�O � MAIS UTILIZADA! ELA SER� MANTIDA PORQUE TALVEZ NO FUTURO ELA SEJA NECESS�RIA
//
int classificacao_h_alfa_gpu(IDL_LONG ja_start, IDL_LONG ja_fim, IDL_LONG ih_start, 
	IDL_LONG ih_fim, IDL_LONG dimx, IDL_LONG dimy, float *alfa, float *entropia, 
	float *anisotropia, IDL_INT *imagem_h_a_0, IDL_INT *imagem_h_a_1, IDL_INT *imagem_h_a_2,
	IDL_INT *imagem_h_a_ani_0, IDL_INT *imagem_h_a_ani_1, IDL_INT *imagem_h_a_ani_2) {
	
	// Cria as vari�veis na GPU
    float *alfa_d, *entropia_d, *anisotropia_d;
	IDL_INT *imagem_h_a_0_d, *imagem_h_a_1_d, *imagem_h_a_2_d;
	IDL_INT *imagem_h_a_ani_0_d, *imagem_h_a_ani_1_d, *imagem_h_a_ani_2_d;

    // Aloca espa�o na GPU para as vari�veis
    float elementos = (float) dimx * dimy;
    cudaMalloc(&imagem_h_a_0_d, elementos * sizeof(IDL_INT));
    cudaMalloc(&imagem_h_a_1_d, elementos * sizeof(IDL_INT));
    cudaMalloc(&imagem_h_a_2_d, elementos * sizeof(IDL_INT));
	cudaMalloc(&imagem_h_a_ani_0_d, elementos * sizeof(IDL_INT));
    cudaMalloc(&imagem_h_a_ani_1_d, elementos * sizeof(IDL_INT));
    cudaMalloc(&imagem_h_a_ani_2_d, elementos * sizeof(IDL_INT));
    cudaMalloc(&alfa_d, elementos * sizeof(float));
    cudaMalloc(&entropia_d, elementos * sizeof(float));
    cudaMalloc(&anisotropia_d, elementos * sizeof(float));
    
    // Copia as matrizes para a mem�ria da GPU
    cudaMemcpy(alfa_d, alfa, elementos * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(entropia_d, entropia, elementos * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(anisotropia_d, anisotropia, elementos * sizeof(float), cudaMemcpyHostToDevice);
    
    // Faz o processamento na GPU
    dim3 threadsPerBlock(8,8);
    dim3 numBlocks(dimx/threadsPerBlock.x+1, dimy/threadsPerBlock.y+1);
    classificacao_h_alfa_kernel <<<numBlocks,threadsPerBlock>>>(ja_start, ja_fim, ih_start, ih_fim,
		dimx, dimy, alfa_d, entropia_d, anisotropia_d, imagem_h_a_0_d, imagem_h_a_1_d, imagem_h_a_2_d,
		imagem_h_a_ani_0_d, imagem_h_a_ani_1_d, imagem_h_a_ani_2_d);
    cudaThreadSynchronize();

    // Copia as matrizes para a mem�ria RAM
    cudaMemcpy(imagem_h_a_0, imagem_h_a_0_d, elementos * sizeof(IDL_INT), cudaMemcpyDeviceToHost);
    cudaMemcpy(imagem_h_a_1, imagem_h_a_1_d, elementos * sizeof(IDL_INT), cudaMemcpyDeviceToHost);
    cudaMemcpy(imagem_h_a_2, imagem_h_a_2_d, elementos * sizeof(IDL_INT), cudaMemcpyDeviceToHost);
	cudaMemcpy(imagem_h_a_ani_0, imagem_h_a_ani_0_d, elementos * sizeof(IDL_INT), cudaMemcpyDeviceToHost);
    cudaMemcpy(imagem_h_a_ani_1, imagem_h_a_ani_1_d, elementos * sizeof(IDL_INT), cudaMemcpyDeviceToHost);
    cudaMemcpy(imagem_h_a_ani_2, imagem_h_a_ani_2_d, elementos * sizeof(IDL_INT), cudaMemcpyDeviceToHost);

    // Libera as matrizes armazenadas na GPU
    cudaFree(imagem_h_a_0_d);
    cudaFree(imagem_h_a_1_d);
    cudaFree(imagem_h_a_2_d);
	cudaFree(imagem_h_a_ani_0_d);
    cudaFree(imagem_h_a_ani_1_d);
    cudaFree(imagem_h_a_ani_2_d);
    cudaFree(alfa_d);
    cudaFree(entropia_d);
    cudaFree(anisotropia_d);

    return 0;
}


//
// ESTA FUN��O N�O � MAIS UTILIZADA! ELA SER� MANTIDA PORQUE TALVEZ NO FUTURO ELA SEJA NECESS�RIA
//
//
// Fun��o a ser chamada pelo IDL para fazer a classifica��o da imagem.
// Portei o c�digo do arquivo plano_h_alfa.pro
int classificacao_h_alfa_idl(int argc, void *argv[]) {
	return classificacao_h_alfa_gpu((IDL_LONG) argv[0], (IDL_LONG) argv[1], (IDL_LONG) argv[2], 
		(IDL_LONG) argv[3],	(IDL_LONG) argv[4], (IDL_LONG) argv[5], (float*) argv[6], 
		(float*) argv[7], (float*) argv[8], (IDL_INT*) argv[9], (IDL_INT*) argv[10], 
		(IDL_INT*) argv[11], (IDL_INT*) argv[12], (IDL_INT*) argv[13], (IDL_INT*) argv[14]);
}

/*
 * Essa � a fun��o que realmente executa o processamento dos dados
 *
 * Entradas/sa�das:
 *  - (endere�o de) k0
 *  - (endere�o de) k1
 *  - (endere�o de) k2
 *  - (valor de)    dimx
 *  - (valor de)    dimy
 *  - (valor de)    i_start
 *  - (valor de)    j_start
 *  - (valor de)    i_fim
 *  - (valor de)    j_fim
 *  - (valor de)    jan1
 *  - (valor de)    jan2
 ******** vari�veis para classifica��o ********
 *  - (valor de)    ja_start
 *  - (valor de)    ja_fim
 *  - (valor de)    ih_start
 *  - (valor de)    ih_fim
 *  - (endere�o de) imagem_h_a_0
 *  - (endere�o de) imagem_h_a_1
 *  - (endere�o de) imagem_h_a_2
 *  - (endere�o de) imagem_h_a_ani_0
 *  - (endere�o de) imagem_h_a_ani_1
 *  - (endere�o de) imagem_h_a_ani_2
 *
 * Retorna
 *  - 0 (n�mero inteiro)
 *
 * TODO
 * FUTURAMENTE FORNECER SUPORTE PARA ALGUNS ERROS COMUNS *
 *
 */
int proc_gpu(IDL_COMPLEX *k0, IDL_COMPLEX *k1, IDL_COMPLEX *k2, IDL_LONG dimx, IDL_LONG dimy,
//    float *lamb1, float *lamb2, float *lamb3, float *alfa, float *entropia, float *anisotropia, 
	IDL_LONG i_start, IDL_LONG j_start, IDL_LONG i_fim, IDL_LONG j_fim, IDL_LONG jan1, IDL_LONG jan2,
	// vari�veis para a classifica��o
	IDL_LONG ja_start, IDL_LONG ja_fim, IDL_LONG ih_start, IDL_LONG ih_fim,
	IDL_INT *imagem_h_a_0, IDL_INT *imagem_h_a_1, IDL_INT *imagem_h_a_2,
	IDL_INT *imagem_h_a_ani_0, IDL_INT *imagem_h_a_ani_1, IDL_INT *imagem_h_a_ani_2) {
	
	// Cria as vari�veis na GPU
    IDL_COMPLEX *k0_d, *k1_d, *k2_d;
    //float *lamb1_d, *lamb2_d, *lamb3_d, 
	float *alfa_d, *entropia_d, *anisotropia_d;
	IDL_INT *imagem_h_a_0_d, *imagem_h_a_1_d, *imagem_h_a_2_d;
	IDL_INT *imagem_h_a_ani_0_d, *imagem_h_a_ani_1_d, *imagem_h_a_ani_2_d;

    // Aloca espa�o na GPU para as vari�veis
    float elementos = (float) dimx * dimy;
    cudaMalloc((void**) &(k0_d), elementos * sizeof(IDL_COMPLEX));
    cudaMalloc((void**) &(k1_d), elementos * sizeof(IDL_COMPLEX));
    cudaMalloc((void**) &(k2_d), elementos * sizeof(IDL_COMPLEX));
    //cudaMalloc(&lamb1_d, elementos * sizeof(float));
    //cudaMalloc(&lamb2_d, elementos * sizeof(float));
    //cudaMalloc(&lamb3_d, elementos * sizeof(float));
    cudaMalloc(&alfa_d, elementos * sizeof(float));
    cudaMalloc(&entropia_d, elementos * sizeof(float));
    cudaMalloc(&anisotropia_d, elementos * sizeof(float));	
    
    // Copia as matrizes para a mem�ria da GPU
    cudaMemcpy(k0_d, k0, elementos * sizeof(IDL_COMPLEX), cudaMemcpyHostToDevice);
    cudaMemcpy(k1_d, k1, elementos * sizeof(IDL_COMPLEX), cudaMemcpyHostToDevice);
    cudaMemcpy(k2_d, k2, elementos * sizeof(IDL_COMPLEX), cudaMemcpyHostToDevice);
    
    // Faz os c�lculos na GPU para obter as matrizes H-A-alfa
    dim3 threadsPerBlock(8,8);
    dim3 numBlocks(dimx/threadsPerBlock.x+1, dimy/threadsPerBlock.y+1);
    polsar_h_a_alfa_kernel <<<numBlocks,threadsPerBlock>>>(k0_d, k1_d, k2_d, 
		//lamb1_d, lamb2_d, lamb3_d,
        alfa_d, entropia_d, anisotropia_d, dimx, dimy, i_start, j_start, i_fim, j_fim, jan1, jan2);
    cudaThreadSynchronize();

/*
	// Copia as matrizes para a mem�ria RAM (H-A-alfa e os autovalores)
    cudaMemcpy(lamb1, lamb1_d, elementos * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(lamb2, lamb2_d, elementos * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(lamb3, lamb3_d, elementos * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(alfa,  alfa_d, elementos * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(entropia, entropia_d, elementos * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(anisotropia, anisotropia_d, elementos * sizeof(float), cudaMemcpyDeviceToHost);
*/

	// Libera mem�ria na GPU para alocar as vari�veis para armazenar as matrizes classificadas
	cudaFree(k0_d);
    cudaFree(k1_d);
    cudaFree(k2_d);
    //cudaFree(lamb1_d);
    //cudaFree(lamb2_d);
    //cudaFree(lamb3_d);

	// Aloca matrizes para armazenar as classifica��es das imagens (Entropia e anisotropia)
	cudaMalloc(&imagem_h_a_0_d, elementos * sizeof(IDL_INT));
    cudaMalloc(&imagem_h_a_1_d, elementos * sizeof(IDL_INT));
    cudaMalloc(&imagem_h_a_2_d, elementos * sizeof(IDL_INT));
	cudaMalloc(&imagem_h_a_ani_0_d, elementos * sizeof(IDL_INT));
    cudaMalloc(&imagem_h_a_ani_1_d, elementos * sizeof(IDL_INT));
    cudaMalloc(&imagem_h_a_ani_2_d, elementos * sizeof(IDL_INT));

	// Faz a classifica��o das matrizes na GPU
    classificacao_h_alfa_kernel <<<numBlocks,threadsPerBlock>>>(ja_start, ja_fim, ih_start, ih_fim,
		dimx, dimy, alfa_d, entropia_d, anisotropia_d, imagem_h_a_0_d, imagem_h_a_1_d, imagem_h_a_2_d,
		imagem_h_a_ani_0_d, imagem_h_a_ani_1_d, imagem_h_a_ani_2_d);
    cudaThreadSynchronize();

	// Copia as matrizes classificadas para a mem�ria RAM
    cudaMemcpy(imagem_h_a_0, imagem_h_a_0_d, elementos * sizeof(IDL_INT), cudaMemcpyDeviceToHost);
    cudaMemcpy(imagem_h_a_1, imagem_h_a_1_d, elementos * sizeof(IDL_INT), cudaMemcpyDeviceToHost);
    cudaMemcpy(imagem_h_a_2, imagem_h_a_2_d, elementos * sizeof(IDL_INT), cudaMemcpyDeviceToHost);
	cudaMemcpy(imagem_h_a_ani_0, imagem_h_a_ani_0_d, elementos * sizeof(IDL_INT), cudaMemcpyDeviceToHost);
    cudaMemcpy(imagem_h_a_ani_1, imagem_h_a_ani_1_d, elementos * sizeof(IDL_INT), cudaMemcpyDeviceToHost);
    cudaMemcpy(imagem_h_a_ani_2, imagem_h_a_ani_2_d, elementos * sizeof(IDL_INT), cudaMemcpyDeviceToHost);

    // Libera as matrizes armazenadas na GPU
    cudaFree(alfa_d);
    cudaFree(entropia_d);
    cudaFree(anisotropia_d);
	cudaFree(imagem_h_a_0_d);
    cudaFree(imagem_h_a_1_d);
    cudaFree(imagem_h_a_2_d);
	cudaFree(imagem_h_a_ani_0_d);
    cudaFree(imagem_h_a_ani_1_d);
    cudaFree(imagem_h_a_ani_2_d);

    return 0;
}

// Essa � a fun��o que � chamada pelo IDL. Ela atua como uma esp�cie de parser
int proc_gpu_idl(int argc, void *argv[]) {
	return proc_gpu((IDL_COMPLEX*) argv[0], (IDL_COMPLEX*) argv[1], (IDL_COMPLEX*) argv[2], 
		(IDL_LONG) argv[3],	(IDL_LONG) argv[4], (IDL_LONG) argv[5], (IDL_LONG) argv[6], 
		(IDL_LONG) argv[7], (IDL_LONG) argv[8], (IDL_LONG) argv[9],(IDL_LONG) argv[10],
		// vari�veis para a classifica��o
		(IDL_LONG) argv[11], (IDL_LONG) argv[12], (IDL_LONG) argv[13], (IDL_LONG) argv[14],
		(IDL_INT*) argv[15], (IDL_INT*) argv[16], (IDL_INT*) argv[17],
		(IDL_INT*) argv[18], (IDL_INT*) argv[19], (IDL_INT*) argv[20]);
}