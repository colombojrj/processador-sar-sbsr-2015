#include <stdio.h>
#include <windows.h>
#include "idl_export.h"

double PCFreq = 0.0;
__int64 CounterStart = 0;

/*
 * Fun��o que inicia a contagem de tempo
 * 
 * Entradas: 
 *   argc - deve ser 0
 *   argv[] - n�o tem nenhum endere�o a ser utilizado
 * 
 * Sa�das:
 *   (IDL_LONG) 0 - se tudo OK
 *   (IDL_LONG) 1 - se ocorreu algum problema e n�o foi iniciada a contagem de tempo
 */
IDL_LONG tic (int argc, void *argv[]) {
	LARGE_INTEGER li;
	if (!QueryPerformanceFrequency(&li)) {
		return (IDL_LONG) 1; // Falha!
	}
	PCFreq = double(li.QuadPart)/1000.0;

    QueryPerformanceCounter(&li);
    CounterStart = li.QuadPart;

	return (IDL_LONG) 0; // Ok!
}

/*
 * Fun��o que retorna a contagem de tempo
 * 
 * Entradas: 
 *   argc - deve ser 0
 *   argv[] - n�o tem nenhum endere�o a ser utilizado
 *
 * Sa�da:
 *   (double) tempo - decorrido desde o in�cio da contagem
 */
double toc(int argc, void *argv[]) {
	LARGE_INTEGER li;
    QueryPerformanceCounter(&li);
	return (li.QuadPart-CounterStart)/PCFreq/((double) 1000.0);
}