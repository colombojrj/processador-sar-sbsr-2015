pro polsar_h_a_alfa, k0,k1,k2,dimx,dimy, jan_coer_rang, jan_coer_azim, dirb, $ ;entradas
                     entropia, anisotropia, alfa                               ;saidas

  ; variáveis para o cálculo H-A-alfa
  entropia     = fltarr(dimx, dimy)
  anisotropia  = fltarr(dimx, dimy)
  alfa         = fltarr(dimx, dimy)
  jana1 = (jan_coer_rang-1)/2
  jana2 = (jan_coer_azim-1)/2
  j_start = jana1
  i_start = jana2
  j_fim   = dimy-jana1-1
  i_fim   = dimx-jana2-1
  
  ; variáveis para a classificação
  imagem_h_a    = intarr(3,dimx,dimy) ; h-alfa
  imagem_h_a_0  = intarr(dimx,dimy)   ; h-alfa
  imagem_h_a_1  = intarr(dimx,dimy)   ; h-alfa
  imagem_h_a_2  = intarr(dimx,dimy)   ; h-alfa
  imagem_h_a_ani= intarr(3,dimx,dimy) ; h-alfa_ani
  imagem_h_a_ani_0= intarr(dimx,dimy) ; h-alfa_ani
  imagem_h_a_ani_1= intarr(dimx,dimy) ; h-alfa_ani
  imagem_h_a_ani_2= intarr(dimx,dimy) ; h-alfa_ani
  offset        = 10
  ja_start = offset
  ja_fim   = dimy-offset
  ih_start = offset
  ih_fim   = dimx-offset
  
  ; vetor para armazenar o de execução de cada uma de 100 realizações
  tempo_gasto = fltarr(100)
  
  print, '  2.1. Cáculo da matriz de coerência'
  print, '  2.2. Cáculo da entropia'
  print, '  2.3. Cáculo da anisotropia'
  print, '  2.4. Cáculo do alfa'
  
  func = 'proc_gpu_idl'
  ; meu notebook, diretório do visual studio 2010
  local_dll = 'D:\Documentos\Visual Studio 2010\Projects\IDL\polsar_h_a_alfa_gpu\x64\Release\polsar_h_a_alfa_gpu'
  ; supercomputador
;  local_dll = 'C:\Users\JoseRoberto\Desktop\resultadoGPU\ImplementacaoGPU\exe\polsar_h_a_alfa_gpu'
  for count = 0,99 do begin
    tic
    status = CALL_EXTERNAL(local_dll,func,K0,K1,K2,dimx,dimy,i_start,j_start,i_fim,j_fim,jana1,jana2,ja_start,ja_fim,ih_start,ih_fim,imagem_h_a_0,imagem_h_a_1,imagem_h_a_2,imagem_h_a_ani_0,imagem_h_a_ani_1,imagem_h_a_ani_2,VALUE=[0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0], /UNLOAD, /CDECL, AUTO_GLUE=0)
    tempo_gasto(count)=toc(0)
    print, 'Tempo gasto:', toc()
  endfor
  
  save, tempo_gasto, filename = dirb + 'tempo_gasto.sav'

  ; Copia as imagens classificadas
  imagem_h_a(0, *, *) = imagem_h_a_0(*,*)
  imagem_h_a(1, *, *) = imagem_h_a_1(*,*)
  imagem_h_a(2, *, *) = imagem_h_a_2(*,*)
  imagem_h_a_ani(0, *, *) = imagem_h_a_ani_0(*,*)
  imagem_h_a_ani(1, *, *) = imagem_h_a_ani_1(*,*)
  imagem_h_a_ani(2, *, *) = imagem_h_a_ani_2(*,*)
  
  window,14, xs=dimx/4, ys=dimy/2, title='Classificacao: H - alfa'
  aa = rebin( imagem_h_a, 3, dimx/4,dimy/2 )
  tv, aa, true=1
  
  window,15, xs=dimx/4, ys=dimy/2, title='Classificacao: H - alfa - ani'
  aa = rebin( imagem_h_a_ani, 3, dimx/4,dimy/2)
  tv, aa, true=1
  
return
end
