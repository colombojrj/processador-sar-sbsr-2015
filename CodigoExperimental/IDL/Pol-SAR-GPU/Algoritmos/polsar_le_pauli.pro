pro polsar_le_pauli, dira, dirb, arquivo1, arquivo2, arquivo3, $   ; entradas
                     dimx, dimy, k0, k1, k2                        ; saidas

  ;definir dimensoes
	imagem1 = complexarr(4000,1580 )
	imagem2 = complexarr(4000,1580)
	imagem3 = complexarr(4000,1580);
	
	print , '  1.1. Lendo imagens'
  ;imagem Shh
  openr,2, dira + arquivo1, xdr=1 ; definir local do arquivo
    readu,2,imagem1
	close,2
	
  ;imagem Svv
  openr,2,dira + arquivo2, xdr=1 ; definir local do arquivo
    readu,2,imagem2
	close,2
  
  ;imagem Shv
  openr,2,dira + arquivo3, xdr=1 ; definir local do arquivo
    readu,2,imagem3
	close,2

  print , '  1.2. Calculando Vetor de Pauli'
  k0 = ( imagem1(0:3999, 0:1579)+imagem2(0:3999, 0:1579) )/ sqrt(2.0) ;(Shh + Svv)/sqrt(2)
  k1 = ( imagem1(0:3999, 0:1579)-imagem2(0:3999, 0:1579) )/ sqrt(2.0) ;(Shh - Svv)/sqrt(2)
  k2 = ( 2.0*imagem3(0:3999, 0:1579)                     )/ sqrt(2.0) ;(  2*Shv  )/sqrt(2)
  
  return
end