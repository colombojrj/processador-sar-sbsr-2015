; meu notebook
@D:\Dropbox\SBSR\2014\CodigoExperimental\IDL\Pol-SAR-GPU\Algoritmos\polsar_le_pauli.pro
@D:\Dropbox\SBSR\2014\CodigoExperimental\IDL\Pol-SAR-GPU\Algoritmos\polsar_h_a_alfa.pro
@D:\Dropbox\SBSR\2014\CodigoExperimental\IDL\Pol-SAR-GPU\Algoritmos\plano_h_alfa.pro
@D:\Dropbox\SBSR\2014\CodigoExperimental\IDL\TicToc\tic.pro
@D:\Dropbox\SBSR\2014\CodigoExperimental\IDL\TicToc\toc.pro
; supercomputador
;@C:\Users\JoseRoberto\Desktop\resultadoGPU\Algoritmos\polsar_le_pauli.pro
;@C:\Users\JoseRoberto\Desktop\resultadoGPU\Algoritmos\polsar_h_a_alfa.pro
;@C:\Users\JoseRoberto\Desktop\resultadoGPU\Algoritmos\plano_h_alfa.pro
;@E:\JoseRoberto\tic.pro
;@E:\JoseRoberto\toc.pro
pro polsar_main

;------------------------entradas---------------------------
; meu notebook
dira           = 'D:\Dropbox\AEROSAR\Pol-SAR\Entradas\'
dirb           = 'D:\Dropbox\AEROSAR\Pol-SAR\Saidas\'
; supercomputador
;dira           = 'C:\Users\JoseRoberto\Documents\Pol-SAR-CPU\Entradas\'
;dirb           = 'C:\Users\JoseRoberto\Documents\Pol-SAR-CPU\Saidas\IDL\'

arquivo1       = 'ESAR97HH.DAT'
arquivo2       = 'ESAR97VV.DAT'
arquivo3       = 'ESAR97HV.DAT'
jan_coer_rang  = 9        ; janela de coerencia em range
jan_coer_azim  = 9        ; janela de coerencia em azimute
dimx           = 4000     ; dimensão em pixel em x
dimy           = 1580     ; dimensão em pixel em y
                                                                  

;;________________________________leitura das imagens___________________________________________
print,'1. Lendo as imagens polarimétricas e calculando o vetor espalhamento na base de Pauli.'
polsar_le_pauli, dira, dirb, arquivo1, arquivo2, arquivo3, $  ;entradas
                 dimx, dimy, k0, k1, k2                       ;saidas
;;______________________________________________________________________________________________

;;______________calcula anisotropia, entropia e alfa e já classifica a imagem___________________
print,'2. Calculando os auto valores, a entropia, a anisotropia, ângulos alfa.'
polsar_h_a_alfa, k0,k1,k2,dimx,dimy, jan_coer_rang, jan_coer_azim, dirb,   $ ;entradas
                 entropia, anisotropia, alfa                                 ;saidas
;;______________________________________________________________________________________________

print,'fim.'
end
