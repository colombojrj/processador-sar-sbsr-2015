PRO TIC, verb
  ; Objetivo: 
  ; - Essa função exerce o papel de "parser", isto é, fornece uma interface 
  ;   user-friendly (amigável) para o usuário
  ;
  ; Comentários:
  ; - O símbolo [*] significa qual valor padrão é assumido caso não seja passado nenhum argumento
  ;
  ; Entrada:
  ; - (INT) verb
  ; - ->     Se 1 o IDL pode mostrar na tela eventuais erros ou warnings
  ; - -> [*] Se 0 o IDL não mostra na tela eventuais erros ou warnings
  ;
  ; Autor: José Roberto Colombo Junior
  ; Data: 10/07/2014
  
  ; Localização da DLL tic_toc (NÃO adicione a extensão .dll)
  ; É permitido alterar o nome do arquivo dll, desde que seja atualizado aqui!
  ;local_dll = 'C:\Users\junior\Documents\Visual Studio 2010\Projects\IDL\TicToc\x64\Release\tic_toc'
  local_dll = 'D:\idl_dlls\tic_toc'
  
  ; Verbose? (Talvez forneça alguma ajuda no debug)
  IF N_ELEMENTS(verb) EQ 0 THEN verb = 0
  
  ; Chama a função tic
  status = CALL_EXTERNAL(local_dll,'tic', /I_VALUE, /CDECL, AUTO_GLUE=0,VERBOSE=verb, SHOW_ALL_OUTPUT=verb)
  
  ; Checa se ocorreu algum problema
  IF status EQ 1 THEN PRINT, 'Problema ao executar a funcao tic. Uma boa ideia seria executar tic(1), para acionar o modo verbose'
  
END