FUNCTION TOC, stop_count, verbose 
  ; Objetivo: 
  ; - Essa função exerce o papel de "parser", isto é, fornece uma interface 
  ;   user-friendly (amigável) para o usuário
  ;
  ; Comentários:
  ; - O símbolo [*] significa qual valor padrão é assumido caso não seja passado nenhum argumento
  ;
  ; Entrada:
  ; - (INT) stop_count
  ; - -> [*] Se 1 então a contagem de tempo é interrompida
  ; - ->     Se 0 acontece a contagem permanece
  ; - (INT) verbose
  ; - ->     Se 1 o IDL pode mostrar na tela eventuais erros ou warnings
  ; - -> [*] Se 0 o IDL não mostra na tela eventuais erros ou warnings
  ;
  ; Saída:
  ; - (DOUBLE) tempo
  ;
  ; Autor: José Roberto Colombo Junior
  ; Data: 10/07/2014
  
  ; Localização da DLL tic_toc (NÃO adicione a extensão .dll)
  ; É permitido alterar o nome do arquivo dll, desde que seja atualizado aqui!
  ;local_dll = 'C:\Users\junior\Documents\Visual Studio 2010\Projects\IDL\TicToc\x64\Release\tic_toc'
  local_dll = 'D:\idl_dlls\tic_toc'
  
  ; Encerrar a contagem de tempo?
  if n_elements(stop_count) eq 0 then stop_count = 1
  
  ; Verbose? (Talvez forneça alguma ajuda no debug)
  if n_elements(verbose) eq 0 then verbose = 0

  ; Chama a função toc e retorna o tempo decorrido desde a chamada da função tic
  RETURN, CALL_EXTERNAL(local_dll, 'toc', /D_VALUE, /CDECL, AUTO_GLUE=0, VERBOSE=verbose, SHOW_ALL_OUTPUT=verbose, UNLOAD=stop_count)
  
END